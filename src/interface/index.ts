// 样式位置
export interface positionInterface {
    left?: string,
    top?: string,
    bottom?: string,
    right?: string
}

export interface spriteInterface {
  length: number, // 精灵图的长度
  url: string, // 图片的路径
  width: number, // 图片的宽度
  height: number, // 图片的高度
  scale?: number, // 缩放
  endPosition: positionInterface // 动画结束站的位置
}
export interface dialogueInterface {
  /**
   * 头像
   */
  avatar: string;
  /**
   * 对话框的
   */
  content: string;
}